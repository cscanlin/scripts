import collections
import re

from fuzzywuzzy import fuzz, process

class SmartMap(collections.MutableMapping):
    # https://stackoverflow.com/a/35507591/1883900
    # https://stackoverflow.com/a/3387975/1883900

    def __init__(self, mapping={}, default=None):
        self.store = {self.format_key(k): v for k, v in mapping.items()}
        self.default = default

    def __getitem__(self, key):
        # if self.default, will overide .get(key, default)
        try:
            return self.store[self.find_key(key)]
        except KeyError as e:
            if self.default:
                return self.default
            raise e

    def __setitem__(self, key, value):
        self.store[self.format_key(key)] = value

    def __delitem__(self, key):
        del self.store[self.format_key(key)]

    def __iter__(self):
        return iter(self.store)

    def __len__(self):
        return len(self.store)

    def find_key(self, key):
        return self.format_key(key)

    def format_key(self, key):
        return key

class RegexMap(SmartMap):
    TYPE_MAP = {
        'int': r'\d+',
        'str': r'\w+',
    }

    def find_key(self, key):
        for pattern, value in self.store.items():
            if re.search(pattern, key):
                return pattern

    def format_key(self, key):
        return key.format(**self.TYPE_MAP)

class FuzzyMap(SmartMap):

    def __init__(self, mapping, score_cutoff=0):
        self.score_cutoff = score_cutoff
        super(FuzzyMap, self).__init__(mapping)

    def find_key(self, key):
        matched = process.extractOne(
            query=key,
            choices=self.store.keys(),
            scorer=fuzz.token_sort_ratio,
            score_cutoff=self.score_cutoff,
        )
        return matched[0] if matched else None

class CaseInsensitiveMap(SmartMap):

    def format_key(self, key):
        return key.lower()


if __name__ == '__main__':
    mapping = {
        r'simple': 'test',
        r'digit/{int}/': 'integer',
        r'string/{str}/': 'word',
        r'fuzzed': 'found',
    }

    smart_map = SmartMap(mapping)
    print(smart_map['simple'] == 'test')
    print(smart_map.get('asdfg') is None)
    print(smart_map.get('asdfg', 'def') == 'def')

    default_map = SmartMap(mapping, default='def')
    print(default_map['simple'] == 'test')
    print(default_map['asdfg'] == 'def')
    default_map = SmartMap(default='def')
    print(default_map['simple'] == 'def')
    print(default_map['asdfg'] == 'def')

    regex_map = RegexMap(mapping)
    print(regex_map['simple'] == 'test')
    print(regex_map['digit/123/'] == 'integer')
    print(regex_map.get('digit/abc/') is None)
    print(regex_map.get('string/abc/') == 'word')

    fuzzy_map = FuzzyMap(mapping)
    print(fuzzy_map['simple'] == 'test')
    print(fuzzy_map.get('digit/123/') == 'integer')
    print(fuzzy_map.get('string/abc/') == 'word')
    fuzzy_map.score_cutoff = 80
    print(fuzzy_map.get('string/abc/') is None)
    print(fuzzy_map['fuzzes'] == 'found')

    lower_map = CaseInsensitiveMap(mapping)
    print(lower_map['simple'] == 'test')
    print(lower_map['FUZZED'] == 'found')
    print(lower_map.get('FUZZEDd') is None)
