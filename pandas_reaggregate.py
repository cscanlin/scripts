import pandas as pd

df = pd.DataFrame({
    'a': pd.Series(['a1', 'a2', 'a3']),
    'b': pd.Series(['b1', 'b1', 'b2']),
    'c': pd.Series([1, 2, 3]),
    'd': pd.Series([4, 5, 6]),
})

df = df.groupby(['a', 'b']).sum()
df['c / d'] = df['c'] / df['d']
print(df)  # correct

df = df.groupby(level=['b']).sum()
print(df)  # inccorrect

df['c / d'] = df['c'] / df['d']
print(df)  # correct
