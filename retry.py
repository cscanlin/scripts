from functools import wraps

def retry(f, num_retries=3, retry_on=Exception, call_before_retry=None):
    @wraps(f)
    def wrapper(instance, *args, **kwargs):
        for i in range(num_retries):
            try:
                return f(instance, *args, **kwargs)
            except retry_on as e:
                if call_before_retry:
                    getattr(instance, call_before_retry)()
                exception = e
        raise exception
    return wrapper

class Retry(object):

    def __init__(self, num_retries=3, retry_on=Exception, call_before_retry=None):
        self.num_retries = num_retries
        self.retry_on = retry_on
        self.call_before_retry = call_before_retry

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            for i in range(self.num_retries):
                try:
                    return f(*args, **kwargs)
                except self.retry_on as e:
                    # if call_before_retry, first try running it as a function
                    if self.call_before_retry:
                        try:
                            self.call_before_retry()
                        except TypeError:
                            # otherwise, assume method on instance
                            getattr(args[0], self.call_before_retry)()
                    exception = e
            raise exception
        return wrapper
