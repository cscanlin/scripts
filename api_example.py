import json
import requests

class CacheDB(object):
    def __init__(self):
        self._cache = {}

    def get_data(self, key):
        print('from cache:')
        return self._cache.get(key)

    def add_data(self, key, data):
        self._cache[key] = data

class BaseAPI(object):

    def __init__(self):
        self.cache_db = CacheDB()

    def make_call(self, url, params=None):
        key = json.dumps({'url': url, 'params': params}, sort_keys=True)
        cached = self.cache_db.get_data(key)
        if cached:
            return json.loads(cached)
        else:
            data = requests.get(url, params).json()

        self.cache_db.add_data(key, json.dumps(data))

        return data

class RealService(BaseAPI):

    def __init__(self, *args, **kwargs):
        super(RealService, self).__init__(*args, **kwargs)

    def retrieve_post(self, post_id):
        url = 'https://jsonplaceholder.typicode.com/posts/{}'.format(post_id)
        params = {'a': 1, 'b': 2}
        return self.make_call(url, params)

    def formatted_post_title(self, post_id):
        data = self.retrieve_post(post_id)
        return '{} - {}'.format(data['id'], data['title'].upper())


class MockMixin(object):
    def retrieve_post(self, post_id):
        return {
            'userId': 5,
            'id': post_id,
            'title': 'test title',
            'body': 'test body text',
        }

class MockService(MockMixin, RealService):
    pass


class Service(object):
    def __new__(self, *args, **kwargs):
        use_mock = kwargs.pop('use_mock', False)
        if use_mock:
            return MockService(*args, **kwargs)
        else:
            return RealService(*args, **kwargs)


if __name__ == '__main__':
    real = Service()
    real_title = real.formatted_post_title(12)
    print(real_title)

    mock = Service(use_mock=True)
    mock_title = mock.formatted_post_title(12)
    print(mock_title)

    assert real_title == '12 - IN QUIBUSDAM TEMPORE ODIT EST DOLOREM'
    assert mock_title == '12 - TEST TITLE'
