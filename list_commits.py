from collections import defaultdict
import os
import subprocess
import sys

DEFAULT_REPOSITORIES = ['marvin', 'heliostats', 'devops']

def list_repo_commits(repo_path):
    repo_name = os.path.split(repo_path)[-1]
    arguments = [
        'git',
        '-C ' + repo_path,
        'log',
        '--pretty=format:"%cd || %s"',
        '--date=format:"%Y-%m-%d"',
        '--author=$(git config user.name)',
        '--all',
    ]
    process = subprocess.Popen(' '.join(arguments), shell=True, stdout=subprocess.PIPE)
    split_commits = process.communicate()[0].decode().split('\n')
    return ['{} || {}'.format(repo_name, c) for c in split_commits]

def group_commits_by_date(commits, prepend_repo_name):
    commits_by_date = defaultdict(list)
    if not any(commits):
        return {}
    for commit in commits:
        repo_name, commit_date, message = commit.split(' || ')
        final_msg = '{}: {}'.format(repo_name, message) if prepend_repo_name else message
        commits_by_date[commit_date].insert(0, final_msg)
    return commits_by_date


if __name__ == '__main__':
    prepend_repo_name = True
    if len(sys.argv) > 1:
        repos = sys.argv[1:]
    elif not DEFAULT_REPOSITORIES and os.path.isdir('.git'):
        repos = [os.getcwd()]
        prepend_repo_name = False
    else:
        repos = DEFAULT_REPOSITORIES

    commits = []
    for repo_name in repos:
        repo_path = os.path.join(os.getenv('PROJECT_HOME', ''), repo_name)
        commits += list_repo_commits(repo_path)

    commits_by_date = group_commits_by_date(commits, prepend_repo_name)
    for commit_date, messages in sorted(list(commits_by_date.items())):
        print(commit_date)
        for message in messages:
            print('- {}'.format(message))
        print('\n')
